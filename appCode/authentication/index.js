async function authenticate(email, pass) {
    if (email === null || pass === null){
        return false;
    }
    var models = require('../models');
    const shaCoder = require('sha256');
    const passHash = shaCoder(pass);
    return await models.User.findAll({where: {email: email}}).then(function (users) {
        if (users.length > 0) {
            if (passHash.toLowerCase() === users[0].pass.toLowerCase()) {
                return users[0];
            } else {
                console.log('test', passHash.toLowerCase());
                console.log('pass', users[0].pass.toLowerCase());
                return false
            }
        } else {
            return false;
        }

    });
}

async function athenticateCodeGenerator(user){
    var models = require('../models');
    var uuidv1 = require('uuid/v1');
    var authCode = uuidv1();
    var now = new Date();
    return await models.code
        .build({user_id: user.id, code: authCode, expire: now.setMinutes(now.getMinutes()+1)})
        .save()
        .then(function (e) {
            return authCode;
        }).catch(error => {
            console.log(error.message);
        });
}

async function getAuthToken(authCode){
    var models = require('../models');
    return await models.code.findAll({where: {code: authCode}}).then(function (codes) {
        if(codes.length === 0){
            return false;
        }
        return new Promise(function (resolve, reject) {
            creatToken(codes[0].user_id, models).then(function (token) {
                resolve(token);
            }).catch(function(err){
                reject(err.message);
            });
        })
    })
}

async function creatToken(user_id, models){
    var jwt = require('jsonwebtoken');
    var uuidv4 = require('uuid/v4');
    var now = new Date();
    var token = jwt.sign({ sub: `${uuidv4()}-${user_id}`,
        exp: Math.floor(Date.now() / 1000) + 300, aud: 'jfzhang', iss: 'http://jfzhang.co.uk' },'shhhhh');
    return await models.token
        .build({user_id: user_id, token: token, expire: now.setMinutes(now.getMinutes() + 5)})
        .save()
        .then(function () {
                return token;
            })
        .catch(error=>{console.log(error.message); return false});
}

async function verifyToken(token) {
    var jwt = require('jsonwebtoken');
    return await jwt.verify(token, 'shhhhh', function (err, decoded) {
        if(typeof decoded === 'undefined'){
            return {status: 'fail', msg: 'expired'};
        }
        if(decoded.exp > Math.round(Date.now()/1000)){
            return new Promise(function (resolve, reject) {
                var models = require('../models');
                models.token.findAll({where:{token:token}}).then(function (tokens) {
                    if(tokens.length ===0){
                        reject({status: 'fail', msg: 'not found'});
                    } else {
                        resolve({status: 'pass'});
                    }
                })
            })
        } else {
            return {status: 'fail', msg: 'expired'};
        }
    })
}

async function refreshToken(token) {
    var models = require('../models');
    return await models.token.findAll({where:{token:token}}).then(function (tokens) {
        if(token.length===0){
            return {status:'fail',msg:'no token founded'};
        } else {
            return new Promise(function (resolve, reject) {
                creatToken(tokens[0].user_id,models).then(function (token) {
                    if(!token){
                        reject({status:'fail',msg:'failed to create new token'});
                    } else {
                        resolve({status:'success', token: token});
                    }
                });
            })
        }
    })
}

module.exports = {authenticate, athenticateCodeGenerator, getAuthToken, verifyToken, refreshToken};