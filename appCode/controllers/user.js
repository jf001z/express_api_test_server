const User = require('../models').user;
module.exports = {
    create(req, res) {
        return User
            .create({
                name: req.body.name,
                email: req.body.email,
                pass: req.body.pass,
            })
            .then(User => res.status(201).send(User))
            .catch(error => res.status(400).send(error));
    },
};