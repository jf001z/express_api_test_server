var gulp = require('gulp');
var nodemon = require('gulp-nodemon');

gulp.task('default',function () {
    nodemon({
        script: './bin/www',
        ext: 'js',
        env: {
            PORT:8085
        },
        legacyWatch: true,
        ignore: ['./node_modules/**']
    })
        .on('restart',function () {
            console.log('restarting');
        });
});

// Watch
//gulp.task('watch', ['default'], function() {
//    gulp.watch('./routes' + '/*.js', ['scripts']).on("change", reload);
//    gulp.watch('./src' + '/**/*.js', ['scripts']).on("change", reload);
//    gulp.watch('./public/images' + '/*', ['imgCompression']);
//    gulp.watch(paths.templates + '/**/*.html').on("change", reload);
//});
