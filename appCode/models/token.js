'use strict';
module.exports = (sequelize, DataTypes) => {
  const token = sequelize.define('token', {
      id: {type: DataTypes.INTEGER,
          primaryKey: true,
          allowNull: false,
          autoIncrement: true,
      },
      user_id: DataTypes.INTEGER,
      token: DataTypes.TEXT,
      expire: DataTypes.DATE
  }, {});
  token.associate = function(models) {
    // associations can be defined here
  };
  return token;
};