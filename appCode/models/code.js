'use strict';
module.exports = (sequelize, DataTypes) => {
  const code = sequelize.define('code', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
    },
    user_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    code: {type:DataTypes.STRING,
            allowNull: false},
    expire: DataTypes.DATE
  }, {});
  code.associate = function(models) {
    // associations can be defined here
  };
  return code;
};