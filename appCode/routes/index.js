var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    if(req.query.length > 0) {
        res.json(req.query);
    } else {
        res.redirect('/users/login');
    }
});

router.get('/test', function(req, res, next) {
    res.status(200).json(req.query);
});
module.exports = router;
