var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/login', function(req, res, next) {
    if(req.query.hasOwnProperty('redirectUrl')){
        let redirectUrl = req.query.redirectUrl;
        res.status(200).render('login', { title: 'Login', redirectUrl: redirectUrl });
    } else {
        res.status(200).send('you have to specify the redirect url');
    }
});
router.post('/login', function (req, res, next) {
    const redirectUrl = req.body.redirectUrl;

    var authen = require('../authentication');
    authen.authenticate(req.body.email,req.body.password)
        .then(function (authenticated) {
            if(authenticated){
                console.log('auth redirectUrl', redirectUrl);
                authen.athenticateCodeGenerator(authenticated).then(function (authCode) {
                    res.redirect(redirectUrl + `?athenticateCode=${authCode}`);
                });
            } else {
                res.status(302).redirect("/users/login?redirectUrl=" + redirectUrl);
            }
    });

});

router.post('/getAuthToken', function (req, res) {
    if (!req.body.hasOwnProperty('authenticateCode')){
        res.status(400).send('no authenticateCode provided');
    }
    var authen = require('../authentication');
    authen.getAuthToken(req.body.authenticateCode).then(function (token) {
        if(!token){
            res.json({status: 400,msg: 'error'});
        } else {
            res.json({token: token});
        }
    })
});

router.post('/verify', function (req, res) {
    if (!req.body.hasOwnProperty('token')){
        res.status(400).send('no token provided');
    }
    var authen = require('../authentication');
    authen.verifyToken(req.body.token).then(function (decode) {
        res.json(decode);
    });

});

router.post('/refreshToken', function (req, res) {
    if (!req.body.hasOwnProperty('token')){
        res.status(400).send('no token provided');
    }
    var authen = require('../authentication');
    authen.refreshToken(req.body.token).then(function (token) {
        res.json(token);
    });

});
module.exports = router;
